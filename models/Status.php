<?php

namespace app\models;

use yii\helpers\ArrayHelper;//for Drop Down List
use Yii;

/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $status_name
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_name' => 'Status Name',
        ];
    }
	
	//DropDown List for Status
	public static function getStatuss()
	{
		$allStatuss = self::find()->orderBy(['status_name'=>SORT_ASC])->all();//ORDER BY DESC\ASC
		$allStatussArray = ArrayHelper::
					map($allStatuss, 'id', 'status_name');
		return $allStatussArray;
	}
	
	
	/*public static function getAuthorss()
	{
		$allAuthorss = self::find()->all();
		$allAuthorssArray = ArrayHelper::
					map($allAuthorss, 'id', 'status_name');
		return $allAuthorssArray;
	}*/
}
