<?php

namespace app\models;

use app\models\category;
use app\models\User;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property integer $category
 * @property integer $author
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'author', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'body'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'author' => 'Author',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
			'createUserName' => Yii::t('app', 'נוצר ע"י'),
 'updateUserName' => Yii::t('app', 'נערך ע"י'),  
 
        ];
    }
	
	//BlameableBehavior - automatic update the 4 fields..
public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],

				],
			],
		];
	
    }
	
	
	////////////// functions for blamable behaviors
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getCreateUserName() 
    {
        return $this->createUser ? $this->createUser->fullName : 'לא קיים'; // The fullName will showen in the "created by" field (view.php)
    }

    public function getUpdateUser()
    {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getUpdateUserName() 
    {
        return $this->createUser ? $this->updateUser->fullName : 'לא קיים'; // The fullName will showen in the "update by" field (view.php)
    }
    /////////////
	//End of BlameableBehavior
	
	public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
    }
}
