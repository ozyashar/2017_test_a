<?php

namespace app\models;
use yii\helpers\ArrayHelper;//for Drop Down List
use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $category_name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
        ];
    }
	
	
	//DropDown List for Category
	public static function getCategorys()
	{
		$allCategorys = self::find()->all();
		$allCategorysArray = ArrayHelper::
					map($allCategorys, 'id', 'category_name');
		return $allCategorysArray;
	}
}
