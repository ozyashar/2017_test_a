<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
	
	//////////////////////////////////////////////////// RBAC  /////////////////////////////////////////
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$author = $auth->createRole('author');
		$auth->add($author);
		
		$editor = $auth->createRole('editor');
		$auth->add($editor);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}

	public function actionAuthorpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$updateOwnPost = $auth->createPermission('updateOwnPost');
		$updateOwnPost->description = 'Author can update his own post';
		$auth->add($updateOwnPost);
		
		$createPost = $auth->createPermission('createPost'); 
		$createPost->description = 'Author can create new post';
		$auth->add($createPost);	

		$indexPosts = $auth->createPermission('indexPosts'); 
		$indexPosts->description = 'Author can view all posts';
		$auth->add($indexPosts);			
		
		$viewPost = $auth->createPermission('viewPost'); 
		$viewPost->description = 'Author can view post';
		$auth->add($viewPost);
		
		
	}


	public function actionEditorpermissions()
	{
		$auth = Yii::$app->authManager;

		$updatePosts = $auth->createPermission('updatePosts'); 
		$updatePosts->description = 'editor can update posts';
		$auth->add($updatePosts);	

		$publishePost = $auth->createPermission('publishePost'); 
		$publishePost->description = 'editor can publishe Post';
		$auth->add($publishePost);	

		$deletePost = $auth->createPermission('deletePost'); 
		$deletePost->description = 'editor can delete Post';
		$auth->add($deletePost);
	
	}
	

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createUsers = $auth->createPermission('createUsers'); 
		$createUsers->description = 'admin can crreate new users ';
		$auth->add($createUsers);

		$updateUsers = $auth->createPermission('updateUsers'); 
		$updateUsers->description = 'admin can update users';
		$auth->add($updateUsers);

		$deleteUsers = $auth->createPermission('deleteUsers');  
		$deleteUsers->description = 'admin can delete users';
		$auth->add($deleteUsers);
		
		
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$author = $auth->getRole('author'); 

		$updateOwnPost = $auth->getPermission('updateOwnPost'); 
		$auth->addChild($author, $updateOwnPost);
		
		$createPost = $auth->getPermission('createPost'); 
		$auth->addChild($author, $createPost);		
		
		$indexPosts = $auth->getPermission('indexPosts'); 
		$auth->addChild($author, $indexPosts);

		$viewPost = $auth->getPermission('viewPost'); 
		$auth->addChild($author, $viewPost);

		////////////////////////////////////
		
		$editor = $auth->getRole('editor'); 
		$auth->addChild($editor, $author);
		
		$updatePosts = $auth->getPermission('updatePosts'); 
		$auth->addChild($editor, $updatePosts);

		$publishePost = $auth->getPermission('publishePost'); 
		$auth->addChild($editor, $publishePost);	

		$deletePost = $auth->getPermission('deletePost'); 
		$auth->addChild($editor, $deletePost);	


		///////////////////////////////////////
		
		$admin = $auth->getRole('admin'); 
		$auth->addChild($admin, $editor);	
		
		$createUsers = $auth->getPermission('createUsers'); 
		$auth->addChild($admin, $createUsers);
		
		$updateUsers = $auth->getPermission('updateUsers'); 
		$auth->addChild($admin, $updateUsers);
		
		$deleteUsers = $auth->getPermission('deleteUsers'); 
		$auth->addChild($admin, $deleteUsers);
	
		

	}
	
	//insert Rule
	public function actionAutherrule()
	 {
	 	$auth = Yii::$app->authManager;
		
	 	$updateOwnPost = $auth->getPermission('updateOwnPost');
	 	$auth->remove($updateOwnPost);
		
	 	$rule = new \app\rbac\OwnAuthorRule;
	 	$auth->add($rule);
				
	 	$updateOwnPost->ruleName = $rule->name;		
	 	$auth->add($updateOwnPost);	
	 }

	
	
	
}
