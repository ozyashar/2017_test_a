<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnAuthorRule extends Rule
{
	public $name = 'OwnAuthorRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['post']) ? $params['post']->author == $user : false;
		}
		return false;
	}
}