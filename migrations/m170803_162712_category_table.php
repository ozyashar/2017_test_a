<?php

use yii\db\Migration;

class m170803_162712_category_table extends Migration
{
    public function up()
    {
		 $this->createTable(
            'category',
            [
				'id' => 'pk',
                'category_name' => 'string'	
				
				
				],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
         $this->dropTable('category');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
