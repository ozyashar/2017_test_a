<?php

use yii\db\Migration;

class m170803_153101_post_table extends Migration
{
    public function up()
    {
		 $this->createTable(
            'post',
            [
			//integer for every 'Final number' of somthing..
				'id' => 'pk',
                'title' => 'string',	
				'body' => 'string',
				'category' => 'integer',	
                'author' => 'integer',
				'status' => 'integer',
				'created_at'=>'integer',
				'updated_at'=>'integer',
				'created_by'=>'integer',
				'updated_by'=>'integer'	
				
				],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
       $this->dropTable('post');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
