<?php

use app\models\User;
use app\models\Status;//for Drop Down List
use app\models\Category;//for Drop Down List
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->
										dropDownList(Category::getCategorys()) ?>									
										
    <?= $form->field($model, 'author')->
										dropDownList(User::getUsers()) ?>
																						
	<?php  // רצינו את רשימת הכותבי פוסטים לכן הלכנו למודל של יוזר כדי לגשת לפונקציה שתחזיר לנו מערך ממופה של יוזרים?>

    
<?php if (\Yii::$app->user->can('updatePosts', ['user' =>$model]) )  {?>	
	
	<?= $form->field($model, 'status')->
										dropDownList(Status::getStatuss()) ?>

<?php } //  published: if you can do 'update post', so you can see the status?>	  
  
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
